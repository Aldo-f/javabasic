package extra;

import java.util.HashMap;

public class Kaartspel {

    /**
     * Dit kaartspel bestaat uit de kaarten 1 tot en met 10, de boer vrouw en koning worden niet gebruikt.
     * In totaal zijn er dus 40 kaarten per kaartspel.
     * Een hoopje kaarten mag weg wanneer het totaal 10 is:
     * 1 + 9, 2 + 8 en 10 bijvoorbeeld
     * Maximaal mogen er 9 kaarten liggen, behalve op het einde mag er 1 kaart over zijn
     * (als deze een paar vormt met degene die er al liggen)
     * Dus op het einde van het spel liggen er maximaal 9 hoopjes kaarten + evt. 1 extra kaart.
     */

    public Kaartspel() {
        /* maak een wilkeurig geordend kaartspel */
        HashMap<Integer, Integer> cards = new HashMap<>();
        for (int i = 1; i <= 10; i++) {
            cards.put( i, 4 );
        }

        StartSpel();
    }

    public static void main(String[] args) {
        new Kaartspel();
    }

    private void StartSpel() {

    }
}
