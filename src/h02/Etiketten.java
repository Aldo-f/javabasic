package h02;

import javax.swing.*;

public class Etiketten extends JFrame {
    public Etiketten() {
        JFrame venster = new JFrame();
        venster.setSize(300, 250);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Etikettencalculator");
        venster.setLocation(100, 100);
        /* from Verwisselpaneel */
        venster.add(new Etikettenpaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new Etiketten();
    }
}

