package h02;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Verwisselpaneel extends JPanel implements ActionListener {
    private JTextField veld1;
    private JTextField veld2; // input fields
    private JButton actieknop; // can be local, but will keep it all together
    private final int WIDTH_TEXT_FIELD = 13; //declare and initialise/assign at the same time

    public Verwisselpaneel() {
        actieknop = new JButton("Verwissel");
        actieknop.addActionListener(this);
        /* width of the field */
        veld1 = new JTextField("Voornaam", WIDTH_TEXT_FIELD); //ruimte voor een naam of een woord
        veld2 = new JTextField("Dit stond rechts", WIDTH_TEXT_FIELD);
        /* add some tooltips */
        veld1.setToolTipText("Vul hier eventueel iets in");
        actieknop.setToolTipText("Als u hier klikt wordt het woord verwisseld van plaats");
        veld2.setToolTipText("Vul hier eventueel iets in");
        /* add the fields that you made to the panel */
        add(veld1);
        add(actieknop);
        add(veld2);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String veldinhoud1 = veld1.getText();
        String veldinhoud2 = veld2.getText();
        veld1.setText(veldinhoud2);
        veld2.setText(veldinhoud1);
    }
}
