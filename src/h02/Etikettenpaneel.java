package h02;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Etikettenpaneel extends JPanel implements ActionListener {
    private final int WIDTH_TEXT_FIELD = 5; //declare and initialise/assign at the same time
    private JTextField aantalEtikettenVeld;
    private JTextField etikettenPerBladVeld; // input fields
    private JButton berekenknop; // can be local, but will keep it all together
    private JTextArea resultaatveld;

    public Etikettenpaneel() {
        berekenknop = new JButton("Bereken resultaat");
        berekenknop.addActionListener(this);
        /* width of the field */
        aantalEtikettenVeld = new JTextField("100", WIDTH_TEXT_FIELD); //ruimte voor een naam of een woord
        etikettenPerBladVeld = new JTextField("5", WIDTH_TEXT_FIELD);
        // textArea van 6 regels en 25 width
        resultaatveld = new JTextArea(6, 25);
        // Add element to panel in correct order
        add(new JLabel("Aantal etiketten dat u wilt afdrukken"));
        add(aantalEtikettenVeld);
        add(new JLabel("aantal etiketten op een blad"));
        add(etikettenPerBladVeld);
        add(berekenknop);
        add(resultaatveld);
    }

    private void toonResultaat(int aantalEtiketten, int etikettenPerBlad) {
        int vellenNodig = ((aantalEtiketten - 1) / etikettenPerBlad) + 1;
        int over = vellenNodig * etikettenPerBlad - aantalEtiketten;
        String resultaat = "Aantal vellen nodig: " + vellenNodig + "\n" + "Aantal etiketten onbedrukt: " + over;
        /* let text automatically wrap to next line */
        resultaatveld.setLineWrap(true);
        resultaatveld.setWrapStyleWord(true);
        /* set result */
        resultaatveld.setText(resultaat);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int aantalEtiketten = Integer.parseInt(aantalEtikettenVeld.getText()); // set text to int with parseInt
        int etikettenPerBlad = Integer.parseInt(etikettenPerBladVeld.getText());
        toonResultaat(aantalEtiketten, etikettenPerBlad);
    }
}