package h02;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Btwpaneel extends JPanel implements ActionListener {
    private final int WIDTH_TEXT_FIELD = 3; //declare and initialise/assign at the same time
    private JTextField bedragIncBtw;
    private JTextField btwPercentage; // input fields
    private JButton berekenknop; // can be local, but will keep it all together
    private JTextArea resultaatveld;

    public Btwpaneel() {
        berekenknop = new JButton("Bereken btw");
        berekenknop.addActionListener(this);
        /* width of the field */
        bedragIncBtw = new JTextField("200", WIDTH_TEXT_FIELD); //ruimte voor een naam of een woord
        btwPercentage = new JTextField("20", WIDTH_TEXT_FIELD);
        // textField of 40
        resultaatveld = new JTextArea(3, 20);
        // Add element to panel in correct order
        add(new JLabel("Bedrag inclusief btw"));
        add(bedragIncBtw);
        add(new JLabel("Btw-percentage"));
        add(btwPercentage);
        add(new JLabel("%"));
        add(berekenknop);
        add(resultaatveld);
    }


    private void toonResultaat(int bedragIncBtw, int btwPercentage) {
        double totaalPercentage = 100 + btwPercentage;
        double bedragExcBtw = 100 / totaalPercentage * bedragIncBtw;
        double bedragBtw = bedragIncBtw - bedragExcBtw;
        /* resultaat string */
        String resultaat = "Bedrag exclusief btw: " + String.format("%.2f", bedragExcBtw) // afronden op 2 cijfers
                + "\nBedrag van de btw: " + String.format("%.2f", bedragBtw);
        /* set result */
        resultaatveld.setText(resultaat);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int bedragIncBtw = Integer.parseInt(this.bedragIncBtw.getText()); // set text to int with parseInt
        int btwPercentage = Integer.parseInt(this.btwPercentage.getText());
        // send date away
        toonResultaat(bedragIncBtw, btwPercentage);
    }
}