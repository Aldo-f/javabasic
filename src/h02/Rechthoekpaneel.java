package h02;

import javax.swing.*;
import java.awt.*;

public class Rechthoekpaneel extends JPanel {
    /* teken rechthoeken zodaning dat LIFT te zien is */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        /* Change color (before drawing) */
        Color paars = new Color(49, 27, 146);
        g.setColor(paars);
        this.setBackground(new Color(255, 0, 0));
        /* Draw the figures */
        g.fillRect(10, 10, 30, 130);

        g.fillRect(60, 10, 70, 110);
        g.fillRect(110, 120, 20, 20);

        g.fillRect(150, 10, 20, 130);

        g.fillRect(190, 30, 90, 30);
        g.fillRect(210, 60, 70, 20);
        g.fillRect(190, 80, 90, 60);
        g.fillRect(240, 10, 10, 20);

        g.fillRect(300, 30, 60, 110);
        g.fillRect(330, 10, 30, 20);
    }
}