package h02;

import javax.swing.*;

public class Rechthoek extends JFrame {
    public Rechthoek() {
        JFrame venster = new JFrame();
        venster.setSize(390, 190);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Optische illusie");
        venster.setLocation(100, 100);
        /* from Eindtijdpaneel */
        venster.add(new Rechthoekpaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new Rechthoek();
    }
}

