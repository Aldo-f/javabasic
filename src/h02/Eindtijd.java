package h02;

import javax.swing.*;

public class Eindtijd extends JFrame {
    public Eindtijd() {
        JFrame venster = new JFrame();
        venster.setSize(250, 200);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Eindtijd bepalen");
        venster.setLocation(100, 100);
        /* from Eindtijdpaneel */
        venster.add(new Eindtijdpaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new Eindtijd();
    }
}

