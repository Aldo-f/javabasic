package h02;

import javax.swing.*;

public class Btw extends JFrame {
    public Btw() {
        JFrame venster = new JFrame();
        venster.setSize(270, 200);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Btw berekenen");
        venster.setLocation(100, 100);
        /* from Btwpaneel */
        venster.add(new Btwpaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new Btw();
    }
}

