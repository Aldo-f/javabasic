package h02;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Eindtijdpaneel extends JPanel implements ActionListener {
    private final int WIDTH_TEXT_FIELD = 2; //declare and initialise/assign at the same time
    private JTextField startTijdUur, startTijdMin;
    private JTextField tijdsduurInMinuten; // input fields
    private JButton berekenknop; // can be local, but will keep it all together
    private JTextField resultaatveld;
    // TODO: validate correct input 23:59 and tijdsduurInMin 10-120 min
    //private final String HOURS_PATERN = "([01]?[0-9]|2[0-3])";
    //private final String MINUTE_PATERN = "[0-5][0-9]";

    public Eindtijdpaneel() {
        berekenknop = new JButton("Bereken eindtijd");
        berekenknop.addActionListener(this);
        /* width of the field */
        startTijdUur = new JTextField("0", WIDTH_TEXT_FIELD); //ruimte voor een naam of een woord
        startTijdMin = new JTextField("0", WIDTH_TEXT_FIELD); //ruimte voor een naam of een woord
        tijdsduurInMinuten = new JTextField("50", WIDTH_TEXT_FIELD);
        // textField of 40
        resultaatveld = new JTextField(20);
        // Add element to panel in correct order
        add(new JLabel("Begintijd Uren"));
        add(startTijdUur);
        add(startTijdMin);
        add(new JLabel("Tijdsduur in minuten"));
        add(tijdsduurInMinuten);
        add(berekenknop);
        add(resultaatveld);
    }


    private void toonResultaat(int startTijdUur, int startTijdMin, int tijdsduurInMin) {
        /* tijd in naar min. omzetten en optellen bij het aantal min. */
        int startTijdInMin = startTijdUur * 60 + startTijdMin;
        int eindTijdInTotaalAantalMin = startTijdInMin + tijdsduurInMin;
        /* Omzetten van eindtijdInMin. naar een aantal uur + een aantal min.*/
        int eindTijdInUur = eindTijdInTotaalAantalMin / 60;
        int eindTijdInMin = eindTijdInTotaalAantalMin % 60;

        /* > 24h */
        if (eindTijdInUur >= 24) {
            eindTijdInUur = eindTijdInUur % 24;
        }

        /* resultaat string */
        String resultaat = "De eindtijd is: " + eindTijdInUur + "u" + eindTijdInMin;
        /* set result */
        resultaatveld.setText(resultaat);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int startTijdUur = Integer.parseInt(this.startTijdUur.getText()); // set text to int with parseInt
        int startTijdMin = Integer.parseInt(this.startTijdMin.getText());
        int tijdsduurInMin = Integer.parseInt(tijdsduurInMinuten.getText());
        toonResultaat(startTijdUur, startTijdMin, tijdsduurInMin);
    }
}