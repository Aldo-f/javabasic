package h02;

import javax.swing.*;

public class Verwissel extends JFrame {
    public Verwissel() {
        JFrame venster = new JFrame();
        venster.setSize(500, 200);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Verwissel");
        venster.setLocation(100, 100);
        /* from Verwisselpaneel */
        venster.add(new Verwisselpaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new Verwissel();
    }
}

