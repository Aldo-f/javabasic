package h02;

import javax.swing.*;

public class Bal extends JFrame {
    public Bal() {
        JFrame venster = new JFrame();
        venster.setSize(800, 600);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Bal");
        venster.setLocation(100, 100);
        /* from Balpaneel */
        venster.add(new Balpaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new Bal();
    }
}

