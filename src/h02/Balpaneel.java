package h02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Balpaneel extends JPanel implements ActionListener {
    private JTextField grootte;
    private JButton berekenknop; // can be local, but will keep it all together
    // Constanten
    private final int VASTE_AFSTAND = 30; // 30 pixels van linkerkant, onderkant en rechterkant
    private Color KLEUR_BAL = new Color(250, 131, 32); // color of a basketbal


    public Balpaneel() {
        berekenknop = new JButton("Teken");
        berekenknop.addActionListener(this);

        // startwaarde
        grootte = new JTextField("400", 3);
        // Teken de eerste keer de bal met de startGrootte van de bal

        add(new JLabel("Grootte"));
        add(grootte);
        add(berekenknop);
    }

    /* teken de lijn onderaan de kader op VASTE_AFSTAND afstand afhankelijk van de grootte van de kader
    *  en de bal
    *  */
    public void paintComponent(Graphics g) {
        /* Teken lijn */
        super.paintComponent(g);
        int lijnY = getHeight() - VASTE_AFSTAND; // op getHeight() - VASTE_AFSTAND afstand van de kader
        int lijnX2 = getWidth() - (VASTE_AFSTAND); // op getWidth() -  VASTE_AFSTAND is de lengte
        int lijnY2 = lijnY; //for readability, this is a horizontal line
        g.drawLine(30, lijnY, lijnX2, lijnY2);

        /* Teken bal */
        // Kleur
        g.setColor(KLEUR_BAL);
        // get value from JTextField
        int grootte = bepaalGrootte();
        // cirkel
        int middenX = getWidth() / 2;
        int balX = middenX - (grootte / 2); // middenX - (grootte / 2)
        int balY = lijnY - grootte; // lijnY - grootte
        g.fillArc(balX, balY, grootte, grootte, 0, 360);
        // ovaal, moet na het tekenen van de bal, anders komt de bal boven de lijnen
        g.setColor(Color.BLACK);
        g.drawArc(balX + (grootte / 4), balY, grootte / 2, grootte, 0, 360);
        // omtrek
        g.drawArc(balX, balY, grootte, grootte, 0, 360);
    }

    private int bepaalGrootte() {
        return Integer.parseInt( grootte.getText() );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }
}