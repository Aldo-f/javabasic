package h04;

import javax.swing.*;
import java.awt.*;

public class VerplaatsbareBalPaneel extends JPanel {
    public final int BALDIAMETER = 100;

    private int verticalePlaats = 50;
    private int horizontalePlaats = 250;//ongeveer midden

    /* get */
    public int getHorPlaats() {
        return horizontalePlaats;
    }

    public int getVerPlaats() {
        return verticalePlaats;
    }

    /* set */
    public void setHorPlaats(int NieuweHorPlaats) {
        horizontalePlaats = NieuweHorPlaats;
    }


    public void setVerPlaats(int NieuweVerPlaats) {
        verticalePlaats = NieuweVerPlaats;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        /* Teken bal */
        g.setColor(new Color(250, 131, 32));
        g.fillArc(horizontalePlaats, verticalePlaats, BALDIAMETER, BALDIAMETER, 0, 360);
        g.setColor(Color.BLACK);
        g.drawArc(horizontalePlaats + (BALDIAMETER / 4), verticalePlaats, BALDIAMETER / 2, BALDIAMETER, 0, 360);
        g.drawArc(horizontalePlaats, verticalePlaats, BALDIAMETER, BALDIAMETER, 0, 360);
    }
}