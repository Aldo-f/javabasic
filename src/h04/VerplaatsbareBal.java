package h04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VerplaatsbareBal extends JFrame implements ActionListener {
    h04.VerplaatsbareBalPaneel bal;
    private JButton naarLinks, naarRechts, naarBoven, naarOnder;

    public VerplaatsbareBal() {
        JFrame venster = new JFrame();
        venster.setSize(700, 250);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Verplaatsbare bal - versie 2");
        venster.setLocation(100, 100);
        /* create object, buttons and place them */
        bal = new h04.VerplaatsbareBalPaneel();
        venster.add(bal, BorderLayout.CENTER);
        // buttons
        naarLinks = new JButton("<");
        naarLinks.addActionListener(this);
        venster.add(naarLinks, BorderLayout.WEST);//links
        naarRechts = new JButton(">");
        naarRechts.addActionListener(this);
        venster.add(naarRechts, BorderLayout.EAST);//rechts
        naarBoven = new JButton("Naar boven");
        naarBoven.addActionListener(this);
        venster.add(naarBoven, BorderLayout.NORTH);//boven
        naarOnder = new JButton("Naar onder");
        naarOnder.addActionListener(this);
        venster.add(naarOnder, BorderLayout.SOUTH);//onder
        /* Show */
        venster.setVisible(true);
    }

    /* bij een klik de horizontalePlaats aanpassen én opnieuw tekenen van de bal */
    @Override
    public void actionPerformed(ActionEvent e) {
        final int VERPLAATSING = 13; // een verplaatsing van 13 pixels

        //TOD0: Oef 4.4 Controle of dat de bal buiten het vak zou zitten door de verplaatsing

        /* Hierbinnen mag de bal bewegen */
        int diameter = bal.BALDIAMETER;
        int maxHorVerplaatsing = bal.getWidth() - diameter;
        int maxVerVerplaatsing = bal.getHeight() - diameter;
        int nieuwePlaats;

        /* verplaatsing en controle of het mag verplaats worden */
        if (e.getSource() == naarLinks) {
            nieuwePlaats = bal.getHorPlaats() - VERPLAATSING;
            if (nieuwePlaats >= 0) {
                bal.setHorPlaats(nieuwePlaats);
            }
        } else if (e.getSource() == naarRechts) {
            nieuwePlaats = bal.getHorPlaats() + VERPLAATSING;
            if (nieuwePlaats <= maxHorVerplaatsing) {
                bal.setHorPlaats(nieuwePlaats);
            }
        } else if (e.getSource() == naarBoven) {
            nieuwePlaats = bal.getVerPlaats() - VERPLAATSING;
            if (nieuwePlaats >= 0) {
                bal.setVerPlaats(nieuwePlaats);
            }
        } else {
            nieuwePlaats = bal.getVerPlaats() + VERPLAATSING;
            if (nieuwePlaats <= maxVerVerplaatsing) {
                bal.setVerPlaats(nieuwePlaats);
            }
        }
        bal.repaint(); // De *bal* wordt opnieuw getekent
    }

    public static void main(String[] args) {
        new VerplaatsbareBal();
    }
}

