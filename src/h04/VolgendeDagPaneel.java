package h04;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VolgendeDagPaneel extends JPanel implements ActionListener {
    private JTextField dagVeld, maandVeld, jaarVeld;
    private JButton actieknop; // can be local, but will keep it all together
    private JTextField resultaatVeld;

    public VolgendeDagPaneel() {
        /* reageren op muis en enter */
        actieknop = new JButton("Volgende dag");
        actieknop.addActionListener(this);
        dagVeld = new JTextField("21", 2); //ruimte voor een naam of een woord
        dagVeld.addActionListener(this);
        dagVeld.setToolTipText("De dag in geheel getal"); // tooltip
        maandVeld = new JTextField("08", 2); //ruimte voor een naam of een woord
        maandVeld.addActionListener(this);
        maandVeld.setToolTipText("De maand in geheel getal"); // tooltip
        jaarVeld = new JTextField("2017", 4); //ruimte voor een naam of een woord
        jaarVeld.addActionListener(this);
        jaarVeld.setToolTipText("Het jaar in geheel getal"); // tooltip
        resultaatVeld = new JTextField(30);

        /* add the fields that you made to the panel */
        add(new JLabel("Dag"));
        add(dagVeld);
        add(new JLabel("Maand"));
        add(maandVeld);
        add(new JLabel("Jaar"));
        add(jaarVeld);
        add(actieknop);
        add(resultaatVeld);
    }

    public String maakResultaattekst(int dag, int maand, int jaar) {
        //Huidige Dag
        System.out.println("----------------------\nVandaag ");
        System.out.println("Dag = " + dag);
        System.out.println("Maand = " + maand);
        System.out.println("Jaar = " + jaar);

        /* maak new object */
        Kalendergegevens kalendervraagbaak = new Kalendergegevens();

        /* voor de dag van de week te weten */
        String dagVanDeWeek = kalendervraagbaak.dagVanDeWeek(dag, maand, jaar);

        String resultaat = "";
        if (bestaanbareDatum(dag, maand, jaar)) {
            resultaat = "De dag na " + dagVanDeWeek + " " + dag + "/" + maand + "/" + jaar + " is ";
            dag++; // één dag verder
            if (dag > kalendervraagbaak.getAantalDagenInMaand(maand, jaar)) {
                dag = 1;
                maand++; //volgende maand
                if (maand > 12) { //nieuw jaar
                    maand = 1;
                    jaar++;
                }
            }
        } else {
            JOptionPane.showMessageDialog(
                    null,
                    "Uw ingevoerde datum (" + dag + "/" + maand + "/" + jaar + ") lijkt niet te bestaan.",
                    "Onjuiste datum", JOptionPane.ERROR_MESSAGE);
            return ""; // deze String kan ook gebruikt worden om een melding te plaatsen in resultaatVeld ipv de MessageDialog
        }

        /* voor de dag van de week te weten */
        dagVanDeWeek = kalendervraagbaak.dagVanDeWeek(dag, maand, jaar);

        //Volgende dag
        System.out.println("----------------------\nMorgen ");
        System.out.println("Dag = " + dag);
        System.out.println("Maand = " + maand);
        System.out.println("Jaar = " + jaar);
        System.out.println("---------------------- ");

        return resultaat + dagVanDeWeek + " " + dag + "/" + maand + "/" + jaar;
    }

    /* Controle of ingevoerde datum een bestaanbare datum is */
    private boolean bestaanbareDatum(int dag, int maand, int jaar) {
        if (jaar < 1582) {
            return false; //Invoering van de Gregoriaanse kalender in 1582
        } else if (((dag > aantalDagenInMaand(maand, jaar)) || (dag < 1)) || ((maand > 12) || maand < 1)) {
            return false; // dag is te groot (of te klein), maand is groter dan 12 of kleiner dan 1
        } else {
            return true;
        }
    }

    private int aantalDagenInMaand(int maand, int jaar) {
        /* maak new object */
        Kalendergegevens kalendervraagbaak = new Kalendergegevens();

        final int APRIL = 4, JUNI = 6, SEPTEMBER = 9, NOVEMBER = 11;
        final int FEBRUARI = 2; // speciaal geval, schrikkeljaar = 29 anders 28 dagen
        int aantalDagen = 31; // meestvoorkomend aantal dagen is 31, anders overschrijven van de waarde
        if (maand == APRIL || maand == JUNI || maand == SEPTEMBER || maand == NOVEMBER) {
            aantalDagen = 30;
        } else if (maand == FEBRUARI) {
            aantalDagen = 28; // meest voorkomend
            if (kalendervraagbaak.isSchrikkeljaar(jaar)) {
                aantalDagen = 29;
            }
        }
        return aantalDagen;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        /* integers uit de tekstvelden halen */
        int invoerdag = Integer.parseInt(dagVeld.getText());
        int invoermaand = Integer.parseInt(maandVeld.getText());
        int invoerjaar = Integer.parseInt(jaarVeld.getText());
        /* toon de resultaatstring */
        String resultaat = maakResultaattekst(invoerdag, invoermaand, invoerjaar);
        resultaatVeld.setText(resultaat);
    }
}
