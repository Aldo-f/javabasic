package h04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AutoBal extends JPanel implements ActionListener {
    public final int BALDIAMETER = 100;
    private final int WACHTTIJD = 50; //event elke # miliseconden
    private int verticalePlaats = 50;
    private int horizontalePlaats = 291;//ongeveer midden
    private int richtingEnSnelheid = +1; //begint naat rechts

    public AutoBal() {
        //maak timer en start hem
        javax.swing.Timer autoklik = new javax.swing.Timer(WACHTTIJD, this);
        autoklik.start();
    }

    //geef de huidige positie
    public int getVerticalePlaats() {
        return verticalePlaats;
    }

    public void setVerticalePlaats(int verticalePlaats) {
        this.verticalePlaats = verticalePlaats;
    }

    public int getRichtingEnSnelheid() {
        return richtingEnSnelheid;
    }

    public void setRichtingEnSnelheid(int richtingEnSnelheid) {
        this.richtingEnSnelheid = richtingEnSnelheid;
    }

    public int getHorizontalePlaats() {
        return horizontalePlaats;
    }

    public void setHorizontalePlaats(int horizontalePlaats) {
        this.horizontalePlaats = horizontalePlaats;
    }


    /* bij een klik de horizontalePlaats aanpassen (of verticalePlaats) én opnieuw tekenen van de bal */
    @Override
    public void actionPerformed(ActionEvent e) {
        setHorizontalePlaats(getHorizontalePlaats() + richtingEnSnelheid);
        repaint();
    }

    /* teken de bal */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        /* Teken bal */
        g.setColor(new Color(250, 131, 32));
        g.fillArc(horizontalePlaats, verticalePlaats, BALDIAMETER, BALDIAMETER, 0, 360);
        g.setColor(Color.BLACK);
        g.drawArc(horizontalePlaats + (BALDIAMETER / 4), verticalePlaats, BALDIAMETER / 2, BALDIAMETER, 0, 360);
        g.drawArc(horizontalePlaats, verticalePlaats, BALDIAMETER, BALDIAMETER, 0, 360);
    }

}
