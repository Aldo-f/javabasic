package h04;

import javax.swing.*;

public class Kalendergegevens {
    public int getAantalDagenInMaand(int maand, int jaar) {
        final int APRIL = 4, JUNI = 6, SEPTEMBER = 9, NOVEMBER = 11;
        final int FEBRUARI = 2; // speciaal geval, schrikkeljaar = 29 anders 28 dagen
        int aantalDagen = 31; // meestvoorkomend aantal dagen is 31, anders overschrijven van de waarde
        if (maand == APRIL || maand == JUNI || maand == SEPTEMBER || maand == NOVEMBER) {
            aantalDagen = 30;
        } else if (maand == FEBRUARI) {
            aantalDagen = 28; // meest voorkomend
            if (isSchrikkeljaar(jaar)) {
                aantalDagen = 29;
            }
        }
        return aantalDagen;
    }

    public boolean isSchrikkeljaar(int jaar) {
        return ((jaar % 4 == 0) && (jaar % 100 != 0)) || (jaar % 400 == 0);
    }

    public String dagVanDeWeek(int dag, int maand, int jaar) {
        final int START_KALENDER = 1582;
        if (jaar >= START_KALENDER) {
            /* start berekening na kleine controle van de input */
            if (maand > 2) {
                maand = maand - 2;
            } else {
                maand = maand + 10;
                jaar = jaar - 1;
            }
            /* splits jaar in eeuw en jaar (1990 --> 19 en 90) */
            int jaar2cijfers = (jaar % 100);
            jaar = jaar / 100;
            int eeuw2cijfers = jaar;
            System.out.println("----------------------");
            System.out.println("jaar2cijfers = " + jaar2cijfers);
            System.out.println("eeuw2cijfers = " + eeuw2cijfers);

            /* verdere berekeningen */
            int A = (13 * maand - 1) / 5;
            int B = jaar2cijfers / 4;
            int C = eeuw2cijfers / 4;
            int D = A + B + C + dag + jaar2cijfers - 2 * eeuw2cijfers + 700;

            int weekdag = D % 7;
            /* Switch dag van de week in de bijhorende String (afkorting)*/
            String dagVanDeWeek;
            switch (weekdag) {
                case 0:
                    dagVanDeWeek = "zondag";
                    break;
                case 1:
                    dagVanDeWeek = "maandag";
                    break;
                case 2:
                    dagVanDeWeek = "dinsdag";
                    break;
                case 3:
                    dagVanDeWeek = "woensdag";
                    break;
                case 4:
                    dagVanDeWeek = "donderdag";
                    break;
                case 5:
                    dagVanDeWeek = "vrijdag";
                    break;
                case 6:
                    dagVanDeWeek = "zaterdag";
                    break;
                default:
                    dagVanDeWeek = "De Switch voor de dag van de week te krijgen is foutief";
                    break;
            }
            System.out.println("dagVanDeWeek = " + dagVanDeWeek);
            return dagVanDeWeek;
        } else {
            JOptionPane.showMessageDialog(null, "Het ingegeven jaartal moet na " + START_KALENDER + " plaats vinden");
            return "";
        }
    }
}
