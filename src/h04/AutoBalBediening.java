package h04;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AutoBalBediening extends JPanel implements ActionListener {

    AutoBal bal;
    JButton wisselRichting;
    JButton verticaal;

    public AutoBalBediening(AutoBal bal) { //constructor
        this.bal = bal;

        wisselRichting = new JButton("Wissel van richting");
        wisselRichting.addActionListener(this);
        add(wisselRichting);

        verticaal = new JButton("Verplaats verticaal");
        verticaal.addActionListener(this);
        add((verticaal));
    }

    // Juiste actie ondernemen bij klikken
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == wisselRichting) {
            bal.setRichtingEnSnelheid(-bal.getRichtingEnSnelheid());
        } else {
            bal.setVerticalePlaats(bal.getVerticalePlaats() + bal.getRichtingEnSnelheid());
        }
    }
}