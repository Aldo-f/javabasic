package h04;

import javax.swing.*;
import java.awt.*;

public class AutoBalOpstart {

    public AutoBalOpstart() {

        JFrame venster = new JFrame();
        venster.setSize(700, 250);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Automatische bal");
        venster.setLocation(100, 100);
        // Bal
        AutoBal bal = new AutoBal();
        venster.add(bal, BorderLayout.CENTER);
        // Bediening
        AutoBalBediening Bedieningspaneel; //knoppen
        Bedieningspaneel = new AutoBalBediening(bal);
        venster.add(Bedieningspaneel, BorderLayout.SOUTH);
        // Toon
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new AutoBalOpstart();
    }
}

