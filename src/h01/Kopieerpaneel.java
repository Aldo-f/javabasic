package h01;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Kopieerpaneel extends JPanel implements ActionListener {
    private JTextArea veld1, veld2; // input fields
    private JButton actieknop; // can be local, but will keep it all together

    public Kopieerpaneel() {
        actieknop = new JButton(">Kopieer>");
        actieknop.addActionListener(this);
        /* width of the field */
        veld1 = new JTextArea(3, 40); //ruimte voor een naam of een woord
        veld2 = new JTextArea(3, 40);
        /* add some tooltips */
        veld1.setToolTipText("Vul in dit vak een woord in");
        actieknop.setToolTipText("Klik om de text van links naar rechts te brengen");
        veld2.setToolTipText("Hier komt een kopie van het linkerveld");
        /* add the fields that you made to the panel */
        add(veld1);
        add(actieknop);
        add(veld2);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        veld2.setText(veld1.getText());
    }
}
