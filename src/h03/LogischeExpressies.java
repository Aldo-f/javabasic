package h03;

public class LogischeExpressies {
    public LogischeExpressies() {
        /* is het jaar een schrikkeljaar?*/
        int jaar = 2000; //Schrikkeljaar

        System.out.println(
                (((jaar % 4) == 0) && ((jaar % 100) != 0)) || ((jaar % 400) == 0)
        );
        System.out.println(
                ((jaar % 4 == 0) && (jaar % 100 != 0)) || (jaar % 400 == 0)
        );
        System.out.println(
                (jaar % 4 == 0 && jaar % 100 != 0) || jaar % 400 == 0
        );
        System.out.println(
                jaar % 4 == 0 && jaar % 100 != 0 || jaar % 400 == 0
        );
    }

    public static void main(String[] args) {
        new LogischeExpressiesKt();
    }
}

