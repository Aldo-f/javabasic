package h03;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VerplaatsbareBalPaneel extends JPanel implements ActionListener {
    private final int BALDIAMETER = 100;
    private final int VERTICALE_PLAATS = 50;
    private final int VERPLAATSING = 10; // de verplaatsing in pixel bij elke actie

    private JButton naarLinks, naarRechts;
    private int horizontalePlaats = 150;

    public VerplaatsbareBalPaneel() {
        /* reageren op muis en enter */
        naarLinks = new JButton( "Naar links" );
        naarLinks.addActionListener( this );
        naarRechts = new JButton( "Naar rechts" );
        naarRechts.addActionListener( this );

        /* add the fields that you made to the panel */
        add( naarLinks );
        add( naarRechts );
    }


    /* nieuwe horizontale plaats berekenen na klikken op links of rechts */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == naarLinks) {
            horizontalePlaats -= VERPLAATSING;
        } else {
            horizontalePlaats += VERPLAATSING;
        }
        repaint();
    }
}
