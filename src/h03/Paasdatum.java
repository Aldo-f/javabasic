package h03;

import javax.swing.*;

public class Paasdatum extends JFrame {
    public Paasdatum() {
        JFrame venster = new JFrame();
        venster.setSize(500, 200);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Paasdag en paasmaand");
        venster.setLocation(100, 100);
        /* from PaasdatumPaneel */
        venster.add(new PaasdatumPaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new Paasdatum();
    }
}

