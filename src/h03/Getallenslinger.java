package h03;

import javax.swing.*;

public class Getallenslinger extends JFrame {
    public Getallenslinger() {
        JFrame venster = new JFrame();
        venster.setSize( 500, 200 );
        venster.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        venster.setTitle( "Getallenslinger" );
        venster.setLocation( 100, 100 );
        /* from LijnenDiagonaalPaneel */
        venster.add( new GetallenslingerPaneel() );
        /* Show */
        venster.setVisible( true );
    }

    public static void main(String[] args) {
        new Getallenslinger();
    }
}

