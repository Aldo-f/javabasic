package h03

class LogischeExpressiesKt {
    init {
        /* is het jaar een schrikkeljaar?*/
        val jaar = 2000 //Schrikkeljaar

        println(
                jaar % 4 == 0 && jaar % 100 != 0 || jaar % 400 == 0
        )
        println(
                jaar % 4 == 0 && jaar % 100 != 0 || jaar % 400 == 0
        )
        println(
                jaar % 4 == 0 && jaar % 100 != 0 || jaar % 400 == 0
        )
        println(
                jaar % 4 == 0 && jaar % 100 != 0 || jaar % 400 == 0
        )
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            LogischeExpressiesKt()
        }
    }
}
