package h03;

import javax.swing.*;

public class LijnenDiagonaal extends JFrame {
    public LijnenDiagonaal() {
        JFrame venster = new JFrame();
        venster.setSize( 500, 200 );
        venster.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        venster.setTitle( "Diagonale lijnen" );
        venster.setLocation( 100, 100 );
        /* from LijnenDiagonaalPaneel */
        venster.add( new LijnenDiagonaalPaneel() );
        /* Show */
        venster.setVisible( true );
    }

    public static void main(String[] args) {
        new LijnenDiagonaal();
    }
}

