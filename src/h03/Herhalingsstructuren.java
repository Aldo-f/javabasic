package h03;

import javax.swing.*;

public class Herhalingsstructuren extends JFrame {
    public Herhalingsstructuren() {
        // 1
        int raadMij = 1;
        for (int stuur = 2; stuur <= raadMij; stuur += 10) {
            raadMij += 100;
        }
        System.out.println( "raadMij is " + raadMij );

        // 2
        raadMij = 1;
        for (int stuur = 4; stuur <= 13; stuur += 10) {
            raadMij += 1;
        }
        System.out.println( "raadMij is " + raadMij );

        // 3
        raadMij = 1;
        for (int stuur = 2; stuur <= 13; stuur += 10) {
            raadMij += 1;
        }
        System.out.println( "raadMij is " + raadMij );

        // 4
        raadMij = 1;
        int stuur = 5;
        while (stuur <= 13) {
            raadMij += 1;
            stuur += 3;
        }
        System.out.println( "raadMij is " + raadMij );
    }

    public static void main(String[] args) {
        new Herhalingsstructuren();
    }
}

