package h03;

import javax.swing.*;

public class VolgendeDag extends JFrame {
    public VolgendeDag() {
        JFrame venster = new JFrame();
        venster.setSize(400, 120);
        venster.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        venster.setTitle("Bepaal volgende dag");
        venster.setLocation(100, 100);
        /* from VolgendeDagPaneel */
        venster.add(new VolgendeDagPaneel());
        /* Show */
        venster.setVisible(true);
    }

    public static void main(String[] args) {
        new VolgendeDag();
    }
}