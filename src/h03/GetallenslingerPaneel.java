package h03;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GetallenslingerPaneel extends JPanel implements ActionListener {
    private final int WIDTH_TEXT_FIELD = 3; //declare and initialise/assign at the same time
    private JTextField getal;
    private JButton actieknop; // can be local, but will keep it all together
    private JTextArea antwoordVeld;

    public GetallenslingerPaneel() {
        /* reageren op muis en enter */
        actieknop = new JButton( "Start berekening" );
        actieknop.addActionListener( this );
        getal = new JTextField( "57", WIDTH_TEXT_FIELD ); //ruimte voor een naam of een woord
        getal.addActionListener( this );
        getal.setToolTipText( "Geheel getal" ); // tooltip
        antwoordVeld = new JTextArea( 5, 30 );

        /* add the fields that you made to the panel */
        add( new JLabel( "Geheel getal" ) );
        add( getal );
        add( actieknop );
        add( antwoordVeld );
    }

    private void berekenGetallenslinger() {
        int getal = getStartGetal();

        StringBuffer stringBuffer = new StringBuffer();//Lege StringBuffer hier al aanmaken

        while (getal != 1) {
            if (getal % 2 == 0) { //even
                getal /= 2;
            } else {
                getal = getal * 3 + 1;
            }
            stringBuffer.append( getal + " " );
        }

        /* let text automatically wrap to next line */
        antwoordVeld.setLineWrap( true );
        antwoordVeld.setWrapStyleWord( true );
        //System.out.println( stringBuffer );
        antwoordVeld.setText( String.valueOf( stringBuffer ) );
    }

    private int getStartGetal() {
        return Integer.parseInt( getal.getText() );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        berekenGetallenslinger();
    }
}
