package h03;

import javax.swing.*;

public class LijnenHorizontaal extends JFrame {
    public LijnenHorizontaal() {
        JFrame venster = new JFrame();
        venster.setSize( 500, 200 );
        venster.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        venster.setTitle( "Horizontale lijnen" );
        venster.setLocation( 100, 100 );
        /* from Verwisselpaneel */
        venster.add( new LijnenHorizontaalPaneel() );
        /* Show */
        venster.setVisible( true );
    }

    public static void main(String[] args) {
        new LijnenHorizontaal();
    }
}

