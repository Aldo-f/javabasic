package h03;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

public class PaasdatumPaneel extends JPanel implements ActionListener {
    private final int WIDTH_TEXT_FIELD = 3; //declare and initialise/assign at the same time
    private JTextField jaartal;
    private JButton actieknop; // can be local, but will keep it all together
    private JTextArea antwoordVeld;

    public PaasdatumPaneel() {
        /* reageren op muis en enter */
        actieknop = new JButton("Bepaal paasdatum");
        actieknop.addActionListener(this);
        jaartal = new JTextField("2000", WIDTH_TEXT_FIELD); //ruimte voor een naam of een woord
        jaartal.addActionListener(this);
        jaartal.setToolTipText("Geef jaartal in als een geheel getal"); // tooltip
        antwoordVeld = new JTextArea(5, 30);

        /* add the fields that you made to the panel */
        add(new JLabel("Jaar"));
        add(jaartal);
        add(actieknop);
        add(antwoordVeld);
    }

    private void berekenPaasdatum() {
        int jaartal = getJaartal();
        int paasmaand = 0;
        int paasdag = 0;
        String strPaasmaand = "";

        StringBuffer stringBuffer = new StringBuffer();//Lege StringBuffer hier al aanmaken

        /* jaartal in de reeks 1900 -2099 */
        if (jaartal < 1900 || jaartal > 2099) {
            //System.out.println(jaartal + " is geen getal in de reeks 1900-2090.");
            showMessageDialog(null, jaartal + " is geen getal in de reeks 1900-2090.", "Foute input", JOptionPane.ERROR_MESSAGE);
            return;
        }

        /* Huidige datum van pasen en de 4 daaropvolgende datums */
        for (int x = 0; x < 5; x++) {
            /* bepaal de rest van de deling, en werk er met verder */
            int A = jaartal % 19;
            int B = jaartal % 4;
            int C = jaartal % 7;
            int D = (19 * A + 24) % 30;
            int E = (5 + 2 * B + 4 * C + 6 * D) % 7;
            int F = D + E - 9;

            if (F > 0) {
                //System.out.println("F = " + F);
                paasmaand = 4;
                if (F == 26) {
                    paasdag = 19;
                } else {
                    if (F == 25 && D == 28) {
                        paasdag = 18;
                    } else {
                        paasdag = F;
                    }
                }
            } else {
                paasmaand = 3;
                paasdag = F + 31;
            }

            /* getalmaand naar lettermaand */
            switch (paasmaand) {
                case 3:
                    strPaasmaand = "maart";
                    break;
                case 4:
                    strPaasmaand = "april";
                    break;
                default:
                    System.out.println("Pasen zou nromaal enkel maar in maart of april kunnnen voorvallen, u hebt een fout ontdekt.");
            }

            // Create buffer with the paasdatum
            stringBuffer.append("In " + jaartal + " valt Pasen op dag " + paasdag + " van " + strPaasmaand + ".\n");
            jaartal++;

        }

        //System.out.println("paasdag = " + paasdag);
        //System.out.println("paasmaand = " + paasmaand);

        /* let text automatically wrap to next line */
        antwoordVeld.setLineWrap(true);
        antwoordVeld.setWrapStyleWord(true);
        System.out.println(stringBuffer);
        antwoordVeld.setText(String.valueOf(stringBuffer));
    }

    private int getJaartal() {
        return Integer.parseInt(jaartal.getText());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        berekenPaasdatum();
    }
}
