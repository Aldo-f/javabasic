package h03;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LijnenDiagonaalPaneel extends JPanel implements ActionListener {
    private final int WIDTH_TEXT_FIELD = 3; //declare and initialise/assign at the same time
    private final int START_HOOGTE = 35;
    private JButton actieknop; // can be local, but will keep it all together
    private JTextField strAfstand;

    public LijnenDiagonaalPaneel() {
        /* reageren op muis en enter */
        actieknop = new JButton( "Teken de lijnen" );
        actieknop.addActionListener( this );
        strAfstand = new JTextField( "5", WIDTH_TEXT_FIELD ); //ruimte voor een naam of een woord
        strAfstand.addActionListener( this );
        strAfstand.setToolTipText( "in pixels" ); // tooltip

        /* add the fields that you made to the panel */
        add( new JLabel( "Afstand tussen de lijnen" ) );
        add( strAfstand );
        add( actieknop );
    }

    public void paintComponent(Graphics g) {
        super.paintComponent( g );
        // variabelen
        g.setColor( Color.RED );
        int hoogte = getHeight();
        int lengte = getWidth();
        int afstand = (int) getAfstand();

        // teken lijnen

        int yPos1 = START_HOOGTE;
        int yPos2 = hoogte;
        while (yPos1 <= hoogte && yPos2 >= START_HOOGTE) { // van START_HOOGTE tot hoogte
            g.drawLine( 0, yPos1, lengte, yPos2 );
            yPos1 += afstand;
            yPos2 -= afstand;
        }
    }

    private double getAfstand() {
        double afstand = Double.parseDouble( strAfstand.getText() );
        return afstand;
    }

    private void melding() {
        JOptionPane.showMessageDialog(
                null,
                "Het getal moet een geheel getal zijn groter dan 0",
                "Onjuist getal",
                JOptionPane.WARNING_MESSAGE
        );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        double afstand = getAfstand();
        if (afstand <= 0) {
            melding();
        } else {
            repaint();
        }
    }
}
