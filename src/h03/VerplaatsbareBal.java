package h03;

import javax.swing.*;

public class VerplaatsbareBal extends JFrame {
    public VerplaatsbareBal() {
        JFrame venster = new JFrame();
        venster.setSize( 426, 200 );
        venster.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        venster.setTitle( "Verplaatsbare bal" );
        venster.setLocation( 100, 100 );
        /* from VerplaatsbareBalPaneel */
        venster.add( new VerplaatsbareBalPaneel() );
        /* Show */
        venster.setVisible( true );
    }

    public static void main(String[] args) {
        new VerplaatsbareBal();
    }
}

